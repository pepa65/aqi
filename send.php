<?php // $1:AQI
// The address the mail is to be send to (no mail is send if unset):
$to='CRICS Teachers <teachers@crics.asia>, Parrish Wessels <athletics@crics.asia>, Secondary students <secondary-students@crics.asia>';
//$to='ppasschier@crics.asia';
$site='aqi.crics.asia';

$aqi=$argv[1];
require 'ratings.php';
$rating=Rating($aqi);

if(!empty($to)){
	$chart=$site.'/chart.png';
	$bg=$rating[2];
	$fg='#fff';
	if($aqi<=200) $fg='#000'; // Orange/Yellow/Green get black numbers
	$from="From: CRICS AQI <aqi@crics.asia>\r\nContent-Type: text/html; charset=utf-8\r\nContent-Transfer-Encoding: 7bit\r\n";
	$subject='[AQI] '.$aqi.' '.$rating[10].' '.$rating[3];
	$body="<html><body>\r\n";
	$body.='<p style="font-size:110%">On '.date("l, F jS \a\\t h:i a").', the ';
	$body.='<b>Air Quality Index</b> at CRICS is:<br>';
	$body.='<span style="color:'.$fg.'; background-color:'.$bg.'"><b> &nbsp; ';
	$body.=$aqi.' - '.$rating[3]." &nbsp; </b></span></p>\r\n\r\n";
	$body.='<h3><u>'.$rating[11]."</u></h3>\r\n\r\n";
	$body.="<h3>RESPONSE MEASURES</h3>\r\n\r\n";
	for($j=4; $j<=9; ++$j){
		$body.='<p><b>'.$headers[$j].'</b><br>'.$rating[$j]."</p>\r\n\r\n";
	}
	$body.='<p>The lastest value can always be viewed at:<br><a href="https://';
	$body.=$site.'">'.$site."</a></p>\r\n<p>The CRICS AQI Response card can be ";
	$body.='found here:<br><a href="https://'.$chart.'">'.$chart."</a></p>\r\n";
	$body.="<p>Blessings on your day!</p>\r\n</body></html>\r\n\r\n";
	$mail=$from.'To: '.$to."\r\nSubject: ".$subject."\r\n\r\n".$body;
	exec("echo '".$mail."' |msmtp -X aqi/msmtp.log -t");
}
?>
