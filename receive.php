<?php
// The HASH that authenticates the KEY in the curl-POST form from send.php:
$sendhash='9c9c59f6425d4f995d4ce82d96f39807d074fffaaa6b73297ddc23c22894d287';
// The location of the log file:
$log='aqi.csv';
// Write aqi & bg or not:
$aqibg=TRUE;

if(!file_exists($log) || filesize($log)==0){
	if(!file_put_contents($log, "Date,Time,AQI,TemperatureC,Humidity,Day\n")){
		print('<h1>Installation error: file "'.$log.'" is unwritable!</h1>');
	}
}
$date=@$_POST['date'];
$time=@$_POST['time'];
$aqi=@$_POST['aqi'];
$temp=@$_POST['temp'];
$hum=@$_POST['hum'];
$day=@$_POST['day'];
if(isset($aqi) && isset($date) && isset($time) && isset($temp) &&
      isset($hum) && isset($day)){ // Complete reading submitted
	require './ratings.php';
	// Record data in the log file
	$line=$date.','.$time.','.$aqi.','.$temp.','.$hum.','.$day."\n";
	file_put_contents($log, $line, FILE_APPEND | LOCK_EX);
	// Check for database
	if(function_exists('mysqli_connect')){
		$user='portalaqi';
		$pw='Revelations2v22portalaqi';
		$db='portal_';
		$port=3306;
		$wpdb=@mysqli_connect(NULL, $user, $pw, $db, $port);
	}
	if(@$wpdb){ // Insert date into database (post_id=11)
		$rating=Rating($aqi);
		$mask=($aqi>50)? 'show':'hidden';
		$meta_id=array(62101,62102,62103,62104,62105,62106,62107,62108,62109,62110,62111,62112,62113);
		$daytime=$day.' '.$time;
		$temphum='🌡 '.$temp.'°C &nbsp; 💧 '.$hum.'%';
		$meta_key=array('value','color','mask','date','day_time','temp_hum','level','response1','response2','response3','response4','response5','response6');
		$meta_value=array($aqi,$rating[2],$mask,$date,$daytime,$temphum,$rating[3],$rating[4],$rating[5],$rating[6],$rating[7],$rating[8],$rating[9]);
		$table='5OYCf_postmeta';
		$update='UPDATE '.$table.' SET meta_key="aqi_';
		foreach($meta_id as $i=>$id){
			$query=$update.$meta_key[$i].'",meta_value="'.$meta_value[$i].'" WHERE meta_id='.$id.';';
			mysqli_query($wpdb, $query);
			print(mysqli_error($wpdb).' -- '.$query."\n");
		}
	}
	// Write static html page
	$rating=Rating($aqi);
	$file='index.html';
	$page='<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<meta http-equiv="refresh" content="60">
<title>CRICS AQI</title>
<meta name="abstract" content="'.$file.'">
<meta name="copyright" content="CRICS">
<meta name="date" content="'.date("Y-m-d").'">
<meta name="reply-to" content="itteam@crics.asia">
<link rel="icon" href="logo.svg">
<link rel="stylesheet" href="style.css">
<body style="background-color:%0">
<div class="aqi">
 <div class="logo">
  <img src="logo.svg" width="100" alt="CRICS logo">
  <p class="logotitle">CRICS<br>AQI
 </div>
 <div class="outer">
  <div class="inner">
   <div class="aqimain">
    <div class="info">
     <p class="aqititle">Air Quality Index
     <p id="aqinumber">%1
     <p class="aqikind">US AQI
     <p class="advisory">%2
     <p class="time">%3 %4
     <p class="time">%5
    </div>
    <div class="temphumid">
     <img src="temperature.svg" alt="thermometer">%6°C<img src="humidity.svg" alt="drop">%7%
    </div>
   </div>
  </div>
  <div class="response">
   <p class="resptitle">Response Measures
';
	$a=array($rating[2],$aqi,$rating[3],$day,$time,$date,$temp,$hum);
	foreach ($a as $j => $val){
		$page=str_replace('%'.$j, $val, $page);
	}
	for($j=4; $j<=9; ++$j){
		$page.='   <p class="respheader">'.$headers[$j]."\n";
		$page.='   <p class="respinfo">'.$rating[$j]."\n";
	}
	$page.="  </div>\n </div>\n</div>\n";
	file_put_contents($file, $page, LOCK_EX);
	if($aqibg){
		//file_put_contents('aqibg', $aqi.';'.$rating[2], LOCK_EX); // Only AQI & bgcolor
		file_put_contents('aqibg', $aqi.';'.$rating[2].';'.$rating[3].';'.$day.' '.$time.';'.$date.';'.$temp.';'.$hum.';'.$rating[4].';'.$rating[5].';'.$rating[6].';'.$rating[7].';'.$rating[8].';'.$rating[9], LOCK_EX);
	}
}
?>
