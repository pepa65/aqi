<?php
$headers=array('','','','','Low Exertion Activity','Recess (15 minutes)','Lunch (40 minutes)','Physical Education (50 minutes)','Athletic Games (1 hour)','Athletic Practice/Training (1-4 hours)');
function Rating($val, $aqi=1):array{
// Returns the ratings-array for the AQI value (PM2.5 if second parameter is 0)
	$ratings=array( // 0:maxAQI, 1:maxPM2.5@EPA24, 2:colour, 3:Level, 4:h1-LowExActivity, 5:h2-Recess, 6:h3-Lunch, 7:h4-PE, 8:h5-Athletics, 9:h6-Training, 10:emoji, 11:message
		array(0,0,'#fff','Not yet submitted','','','','','','','',''),
		array(51,9.1,'#2f1','Good','No restrictions','No restrictions','No restrictions','No restrictions','No restrictions','No restrictions','💚','No restrictions.'), // Green
		array(101,35.5,'#ee1','Moderate','No restrictions','No restrictions','No restrictions','No restrictions','Exceptionally sensitive individuals should limit intense activities.','Exceptionally sensitive individuals should limit intense activities.','💛','Essentially no restrictions.'), // Yellow
		array(151,55.5,'#fa1','Unhealthy for Sensitive Groups','Observe children with asthma or other respiratory problems and move indoors if needed.','Make indoor space available for children with asthma or other respiratory problems.','Make indoor space available for children with asthma or other respiratory problems.','Make indoor space available for children with asthma or other respiratory problems.','Individuals with asthma or other respiratory/cardiovascular illness should be medically managing their condition. Increase rest periods and substitutions to lower breathing rates.','Individuals with asthma or other respiratory/cardiovascular illness should be medically managing their condition. Increase rest periods and substitutions to lower breathing rates.','🧡️','Doors & windows should be closed.'), // Orange
		array(201,125.5,'#f11','Unhealthy','Observe children with asthma or other respiratory problems and move indoors if needed.','Make indoor space available for children with asthma or other respiratory problems.','Light to moderate exercise. Make indoor space available for children with asthma or other respiratory problems.','Light to moderate exercise. Make indoor space available for children with asthma or other respiratory problems.','Consider canceling rescheduling or relocating the event indoors. Observe sensitive children.','Light to moderate exercise or move indoors. Observe sensitive children.','❤️','Doors & windows must be closed.'), // Red
		array(301,225.5,'#818','Very Unhealthy','Outdoor Activities should be avoided.','Indoor recess.','Lunch may be eaten outside. K-6: Outdoor activity should be avoided.','K-6: indoor PE. 7-12: Restrict outdoor activities to light exercise for less than 1 hour. Observe sensitive children.','Consider canceling, rescheduling, or relocating the event indoors. Only proceed with approval from all groups involved. Less than 1 hour. Observe sensitive children.','Light exercise for less than 1 hour. Observe sensitive children.','💜','All activities should be indoors.'), // Purple
		array(401,350.5,'#811','Hazardous','Outdoor Activities should be avoided.','Indoor recess.','Indoor lunch. Student eat in their period 4 classrooms.','Indoor PE.','The event must be cancelled, rescheduled, or relocated indoors.','Practices can only occur indoors.','🖤','All activities must be indoors.'), // Maroon
		array(501,500.5,'#811','Hazardous','Outdoor Activities should be avoided.','Indoor recess.','Indoor lunch. Student eat in their period 4 classrooms.','Indoor PE.','The event must be cancelled, rescheduled, or relocated indoors.','Practices can only occur indoors.','🖤','All activities must be indoors.'), // Maroon
		array(5000,5000,'#811','Hazardous','Outdoor Activities should be avoided.','Indoor recess.','Indoor lunch. Student eat in their period 4 classrooms.','Indoor PE.','The event must be cancelled, rescheduled, or relocated indoors.','Practices can only occur indoors.','🖤','All activities must be indoors.'), // Maroon
	);
	if(!is_numeric($val)){return [];}
	$i=($aqi ? 0 : 1);
	$j=0;
	foreach($ratings as $elm){
		if($val<$elm[$i]){return $ratings[$j];}
		++$j;
	}
	return $ratings[$j];
}
?>
