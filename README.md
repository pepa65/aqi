# aqi
**Display the Air Quality Index from an AirVisualPro on an auto-updating page**

* Required: php-fpm msmtp(for mailing)

## Function
* The readers can go to the website to view the current AQI rating on a
  simple static html page that gets generated every time data gets submitted.
  Alternatively, aqi.html can be shown with for example: `surf -bmptwF aqi.svg`
	This will auto-refresh with javascript, and only needs the (extended) `aqibg`
  information that is served by `received.php`.
* Data is logged in `log/aqi.csv`.
* Data is recorded if the database specified in `receive.php` is accessible.
* Data is automatically submitted by the script on the LAN server.
* Mail will be sent out when called with a special flag.
* File `aqibg` is written for [bulletin](https://gitlab.com/pepa65/bulletin). 
  It needs to be served with the header `Access-Control-Allow-Origin: "*"`.
  The file `.htaccess` shows how to do this in Apache. In Caddy, use this
  directive in `Caddyfile`: `header /aqibg Access-Control-Allow-Origin "*"`

## Deploy
Put `receive.php`, `style.css`, `ratings.php`, `chart.png` and `logo.svg` in
a directory with 777 permissions on a public facing php server (so `aqi.csv`,
`index.html` and `aqibg` can be written (the latter must be read with CORS, so
deploy `.htaccess`, a line in `Caddyfile`, or some other way - this is only
necessary if a 'bulletin' installation depends on it, and writing it can be
turned off by setting `$aqibg` in `receive.php` to `FALSE`).
Put `update_aqi` on a server with access to the AirVisualPro, and call
`update_aqi` every 5 minutes, for example through a crontab line like:
`*/5 * * * * /PATH/update_aqi`

If a mail report needs to be forced, call `update_aqi` with the -m/--mail flag.
The php scripts `send.php` and `ratings.php` need to be in the same directory,
and `.netrc` must be in the home directory of the user. The package `msmtp`
needs to be set up properly! An example msmtp configuration file `msmtprc` is
included, which needs to be in `/etc`.
Set the `$from` and `$to` variables in `send.php` appropriately (`$subject` and
`$body` can be tweaked).

### Variables
* `$aqibg` in `receive.php` is set to `TRUE` to write `aqibg`, set it to
  `FALSE` if that is not necessary (if no 'bulletin' depends on it).
* `$sendhash` in `receive.php` must correspond with `$sendkey` in `update_aqi`.

#### Renew authentication information
A new KEY / HASH pair can be generated like:
`k=$(uuidgen |tr -d -); h=$(echo -n $k |sha256sum); echo -e "$k\n${h% -}"`
and then be put in `update_aqi` and `receive.php` respectively.

### PHP server security
All files on the public facing server could be read-only except `aqi.csv` and
`index.html`. Do never serve `sendauth` and `README.md`!

# scrapeapi
**Scrape and display WorldAQI and AirVisual station readings**

* Required: bc wget grep

Separate bash script to display AQI information in the terminal.
