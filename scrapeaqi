#!/usr/bin/env bash
set +vx
# scrapeaqi - Scrape and display WorldAQI and AirVisual station readings
# Required: bc wget grep

# AirVisual data sources
asite='https://www.airvisual.com'
locs=('chiang-rai-international-christian-school-crics'
	'hyundai-chiang-rai'
	'mareeruk-chiang-rai-school-kindergarten-section'
	'doi-dhammanava-study-center'
	#'natural-resources-and-environment-office'
	#'wiang-phang-kham-mae-sai'
)
city='chiang-rai' country='thailand'
locations=(${locs[@]/#/$country/$city/})
# Or locations can be given in an array with 'country/city/location' elements

# World AQI data sources
# https://aqicn.org/city/ID
# https://aqicn.org/feed/@UID/?token=WAPI
## UID:ID
# 5776: mueang-chiang-rai/
# 1828: thailand/chiangrai/natural-resources-and-environment-office/
### 11443: thailand/chiang-rai-hospital/ # Not correct
### 11373: thailand/chiang-rai/mfu/ # No AQI
### 11554: thailand/chiang-rai/mae-fah-luang-university/ # Unbelievable
## 1832: thailand/chiangrai/maesai-health-office # Too far away
## 9450: thailand/chiangrai---gaia-station-01/ # Too far away
# 9466: thailand/chiangrai---gaia-station-02/
# 9467: thailand/chiangrai---gaia-station-03/
## 9472 thailand/chiangrai---gaia-station-06/ # Too far away
# 9587: thailand/chiangrai---gaia-station-07/
wapi='e46c2a02b64f69c63335d2afb47e630c2a2923a4'
wsite='https://api.waqi.info'
uids='5776 1828 1832 9450 9466 9467 9487 9587'

#waqiq='/map/bounds/?latlng=19.57,99.71,19.97,99.91'
#waqiq='/search/?keyword=Chiangrai'
#waqis=$(wget -qO- "$wsite$waqiq&token=$wapi")
#for uid in $uids
#do
#	aqi=$(grep -o '"uid":'"$uid"',"aqi":"[^"]*' <<<"$waqis") aqi=${aqi##*\"}
#	name=$(grep -o '"uid":'"$uid"',"aqi":"[^"]*","station":{"name":"[^"]*' <<<"$waqis")
#	name=${name##*\"}
#	echo "$uid: $h$name$x"
#	i=$(Rating $aqi)
#	echo "${colours[$i]}${ratings[$i]}$x  AQI: $b$h$aqi$x"
#done

# b:Bold h:Highlight(cyan) x:Cancel
b=$'\e[1m' h=$'\e[36m' x=$'\e[0m'

# AQI ratings data
limits=(51 101 151 201 301 500 999999990)
pm25=(12.1 35.5 55.5 150.5 250.5 500.4 999999999)
pm10=(55 155 255 355 425 604 999999999)
o3=(55 71 86 106 201 400 999999999) # 400 is made up
co=(4.5 9.5 12.5 15.5 30.5 50.4 999999999)
so2=(36 76 186 305 605 1004 999999999)
no2=(54 101 361 650 1250 2049 999999999)
ratings=('Green: Good'
	'Yellow: Moderate'
	'Orange: Unhealthy for Sensitive Groups'
	'Red: Unhealthy'
	'Purple: Very Unhealthy'
	'Maroon: Hazardous')
colours=($b$'\e[32m' $b$'\e[33m' $'\e[33m' $b$'\e[31m' $'\e[35m' $'\e[31m' $b$'\e[30m')
Rating(){ # $1:value $2:var  O:level[0..5]
	local i r
	local -n var
	# If $2 is empty: use AQI limits
	[[ $2 ]] && var=$2 || var=limits
	# If element 6 of $2 doesn't exist, no output plus error
	[[ ${var[6]} ]] || return 1
	for i in 0 1 2 3 4 5
	do
		r=$(echo "$1<${var[$i]}" |bc -l)
		((r)) && echo $i && return
	done
	echo 5
}

# Header output
echo "${h}The Air Quality Index on $(date '+%Y-%m-%d %H:%M:%S')$x:"

# WAQI parsing
vars=('co' 'no2' 'so2' 'o3' 'pm1' 'pm25' 'pm10' 'p' 'h' 'r' 't' 'dew' 'w' 'aqi')
labels=('CO' 'NO₂' 'SO₂' 'O₃' 'PM₁' 'PM₂․₅' 'PM₁₀' 'p' 'hum' 'rain' 'temp' 'dew' 'wind' 'AQI')
units=('' '' '' '' '' '' '' 'hPa' '%' 'mm' '°C' '°C' 'm/s' '')
for uid in $uids
do
	waqi=$(wget -qO- "$wsite/feed/@$uid/?token=$wapi")
	aqi=$(grep -o '"data":{"aqi":[^,]*' <<<"$waqi") aqi=${aqi##*:}
	[[ $aqi = '"-"' ]] && continue
	loc=$(grep -o '],"name":"[^\"]*' <<<"$waqi") loc=${loc##*\"}
	echo "$b$h$uid $loc$x:"
	for i in ${!vars[@]}
	do
		var=${vars[$i]}
		val=$(grep -o "\"$var\":{\"v\":[^}]*" <<<"$waqi")
		val=${val##*:} unit=${units[$i]}
		# If the air pressure is not in hPa, convert from Torr
		[[ $var = p ]] && ((${val%.*}<840)) &&
			val=$(echo "scale=1; $val*1.33322/1" |bc -l)
		if [[ $val ]]
		then
			[[ $var = co || $var = no2 || $var = so2 || $var = o3 || $var = pm25 || $var = pm10 ]] &&
				c=${colours[$(Rating $val)]} || c=$h
			echo -n "${labels[$i]}:$c$val$x$unit "
		fi
	done
	r=$(Rating $aqi)
	echo "AQI: ${colours[$r]}$aqi$x"
done

# AirVisual parsing
for loc in ${locations[@]}
do
	IFS='/' read country city loc <<<"$loc"
	url="$asite/$country/$city/$loc"
	t=$(wget -qO- "$url" |tail -1)
	#s=$(grep -o ',&q;pm25&q;:{&q;aqi&q;:[^,]*,&q;concentration&q;:[^}]*' <<<"$t")
	s=$(grep -o '{&q;aqi&q;:[^,]*,&q;concentration&q;:[^,]*,&q;pollutantName&q;:&q;pm25&q;' <<<"$t")
	head="$city, $country @ $loc:" head=${head//-/ }
	set -- $head
	echo "$b$h${@^}$x"
	[[ -z $s ]] && echo "No data" && continue
	s=${s:11} aqi=${s%%,*} s=${s%,&q;pollutantName&q;:&q;pm25&q;} pm25=${s##*:}
	[[ ${pm25: -2:1} = . ]] || pm25+=.0
	i=$(Rating $aqi)
	c=${colours[$(Rating $pm25 pm25)]}
	echo "PM₂․₅:$c$pm25${x}μg/m³ AQI: ${colours[$i]}$aqi ${ratings[$i]}$x"
done

exit 0
